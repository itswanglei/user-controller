package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
class RoutingApiWithUriTemplateControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private User user;
    private ObjectMapper objectMapper;
    @Autowired
    private RoutingApiWithUriTemplateController controller;
    private List<User> users;

    @BeforeEach
    public void setUp() {
        user = new User("wanglei", 20, null, "wanglei@twuc.com");
        objectMapper = new ObjectMapper();
        users = new ArrayList<>();
    }

    @Test
    void should_return_user() throws Exception {
        String userAsString = "{\"name\":\"wanglei\"}";
        mockMvc.perform(get("/api/user").contentType(MediaType.APPLICATION_JSON_VALUE).content(userAsString))
                .andExpect(jsonPath("$.user_name").value("wanglei"));
    }

    @Test
    void should_create_user() throws Exception {
        String userAsString = "{\"name\":\"wanglei\"}";
        mockMvc.perform(post("/api/user").contentType(MediaType.APPLICATION_JSON_VALUE).content(userAsString))
                .andExpect(jsonPath("$.user_name").value("wanglei"));
    }

    @Test
    void should_status_be_400_when_name_in_request_body_is_null() throws Exception {
        String userAsString = "{\"name\":null}";
        mockMvc.perform(post("/api/user").contentType(MediaType.APPLICATION_JSON_VALUE).content(userAsString))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_status_be_400_when_age_is_younger_than_18() throws Exception {
        String userAsString = "{\"name\":\"wanglei\",\"age\":17}";
        mockMvc.perform(post("/api/user").contentType(MediaType.APPLICATION_JSON_VALUE).content(userAsString))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_status_be_400_when_age_is_older_than_101() throws Exception {
        String userAsString = "{\"name\":\"wanglei\",\"age\":17}";
        mockMvc.perform(post("/api/user").contentType(MediaType.APPLICATION_JSON_VALUE).content(userAsString))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_status_be_400_when_email_format_is_incorrect() throws Exception {
        String userAsString = "{\"name\":\"wanglei\",\"age\":18,\"email\":\"email\"}";
        mockMvc.perform(post("/api/user").contentType(MediaType.APPLICATION_JSON_VALUE).content(userAsString))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_not_return_age() throws Exception {
        String userAsString = "{\"name\":\"wanglei\",\"age\":18}";
        mockMvc.perform(post("/api/user").contentType(MediaType.APPLICATION_JSON_VALUE).content(userAsString))
                .andExpect(content().string("{\"user_name\":\"wanglei\"}"));
    }

    @Test
    void should_not_return_null_field() throws Exception {
        String userAsString = "{\"name\":\"wanglei\",\"email\":null}";
        mockMvc.perform(post("/api/user").contentType(MediaType.APPLICATION_JSON_VALUE).content(userAsString))
                .andExpect(content().string("{\"user_name\":\"wanglei\"}"));
    }

    @Test
    void should_delete_user() throws Exception {
        String userAsString = "{\"name\":\"wanglei\"}";
        mockMvc.perform(delete("/api/user").contentType(MediaType.APPLICATION_JSON_VALUE).content(userAsString))
                .andExpect(content().string("deleted"));
    }

    @Test
    void should_get_users() throws Exception {
        for (int i = 0; i < 3; i++) {
            users.add(user);
        }
        String usersAsString = objectMapper.writeValueAsString(users);

        int result = this.controller.getUsers().size() + users.size();

        mockMvc.perform(get("/api/users").contentType(MediaType.APPLICATION_JSON_VALUE).content(usersAsString))
                .andExpect(jsonPath("$.*", Matchers.hasSize(result)));
    }

    @Test
    void should_create_user_list() throws Exception {
        for (int i = 0; i < 3; i++) {
            users.add(user);
        }
        String usersAsString = objectMapper.writeValueAsString(users);

        int result = this.controller.getUsers().size() + users.size();

        mockMvc.perform(post("/api/users").contentType(MediaType.APPLICATION_JSON_VALUE).content(usersAsString))
                .andExpect(jsonPath("$.*", Matchers.hasSize(result)));
    }
}